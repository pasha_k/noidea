# -*- coding: utf8 -*-
import pymorphy2,re,random

morph = pymorphy2.MorphAnalyzer()

def eliza(s0):
  def tr(w,impr=False):
    if w==u"я":
      if impr:
        return u"тобой"
      else:
        return u"ты"
    if w==u"ты":
      if impr:
        return u"мне"
      else:
        return u"я"
    if w==u"знаешь":
      return u"знаю"
    if w==u"со" and impr:
      return u"с"
    if w==u"мной":
      return u"тобой"
    if w==u"тебя":
      return u"меня"
    if w==u"меня":
      return u"тебя"
    if w==u"тобой":
      return u"мной"
    if w==u"мои":
      return u"твои"
    if w==u"твои":
      return u"мои"
    if w==u"мой":
      return u"твой"
    if w==u"моим":
      return u"твоим"
    if w==u"твой":
      return u"мой"
    if w==u"твоим":
      return u"моим"
    if w==u"тебе":
      return u"мне"
    if w==u"мне":
      return u"тебе"
    if w==u"вы":
      return u"мы"
    if w==u"мы":
      return u"вы"
    if w==u"вaми":
      return u"нами"
    if w==u"нами":
      return u"вами"
    if w==u"вaс":
      return u"нас"
    if w==u"нас":
      return u"вас"
    if w==u"вашим":
      return u"нашим"
    if w==u"нашим":
      return u"вашим"
    if w==u"твоей":
      return u"моей"
    if w==u"моей":
      return u"твоей"
    if w==u"мной":
      return u"тобой"
    if w==u"тобой":
      return u"мной"
    r=w
    p=morph.parse(w)[0]
    if '1per' in p.tag:
      ri=p.inflect({'2per'})
      if ri is not None:
        r=ri.word
    elif 'VERB' in p.tag and 'impr' in p.tag:
      ri=p.inflect({'INFN'})
      if ri is not None:
        r=ri.word
    elif '2per' in p.tag or 'impr' in p.tag:
      ri=p.inflect({'1per'})
      if ri is not None:
        r=ri.word
    return r

    
  def vopr(s2,impr=False):
    s=re.sub("(\S)([.,\!\?:;])",'\\1 \\2',s2)
    s=re.sub("(\S)([.,\!\?:;])",'\\1 \\2',s)
    s=re.sub("([.,\!\?:;])(\S)",'\\1 \\2',s)
    s=" ".join([tr(x,impr=impr) for x in s.split(" ")])
    s=re.sub("(\S)([.,\!\?:;])",'\\1 \\2',s)
    s=re.sub("\s+([.,\!\?:;])",'\\1',s)
    s=re.sub(":\s+([a-z0-9]+):",' :\\1:',s)
#    if re.match("^.*\!*\?\!*$",s):
#      s=re.sub("^(.*[^\!\?])[\!\?]+$",'\\1!',s)
    if re.match("^.*[^\?]\!+$",s):
      s=re.sub("^(.*?)\!+$",'\\1?',s)
    return s[0].upper()+s[1:]
    

  def sn(s1):
    s=s1.strip()
    s=s.lower()
    s=re.sub("\s+",' ',s)
    impr=False
    you=False
    nn=None
    razum=False
    
    give_flg=False
    
    for w in s.split(" "):
      w=re.sub("[;:\-\?\!]+$",'',w)
      p=morph.parse(w)[0]
      if p.normal_form==u"дать" and ( 'impr' in p.tag or 'futr' in p.tag):
        give_flg=True
      else:
        if p.normal_form==u"разумный":
          razum=True
        if give_flg:
          if 'NOUN' in p.tag and 'accs' in p.tag:
            return u"У меня нет "+p.inflect({'gent'}).word
        if 'VERB' in p.tag and 'impr' in p.tag:
          impr=True
        if p.normal_form==u"ты":
          you=True
        if p.normal_form==u"анал":
          nn=w
        if 'NOUN' in p.tag and nn is None:
          nn=w
    if give_flg:
      return u"Не дам!"
    if you and razum:
      return u"Я бот, боты не являются разумной формой жизни"
    
    v1=vopr(s,impr)
    v1s=filter(lambda x: x.isalpha(),v1).upper()
    v2s=filter(lambda x: x.isalpha(),s).upper()
    if nn is None or ( ( s[-1]=="?" or s[-1]=="!" or impr or you )\
          and v1s!=v2s and random.random()>0.5 ):
      v1=re.sub(u"^(Н|н)у я и",u"почему это я",v1)
      #if ( impr or you ) and v1[-1]!="?":
      if v1[-1]==u".":
        v1=v1[:-1]
      if v1[-1]!=u"?":
        v1=v1+u"?"
      if impr:
        v1=u"Зачем "+v1.lower()
      return v1
    else:
      if nn is not None:
        rnd=random.random()
        p=morph.parse(nn)[0]
        if rnd>0.75:
          return u"Что ты хочешь знать о "+p.inflect({'loct'}).word+u"?"
        elif rnd>0.5:
          return u"Тебя интересует "+p.inflect({'nomn','sing'}).word+u"?"
        elif rnd>0.25:
          return u"Часто думаешь о "+p.inflect({'loct'}).word+u"?"
        else:
          return u"Давно интересуешься "+p.inflect({'ablt'}).word+u"?"

    return re.sub('^(.*?)\.*$','\\1?',s)
  
  snts=re.findall('.*?[\.\!\?]+',s0)
  if len(snts)>0:
    r=" ".join([sn(x) for x in snts])
  else:
    r=sn(s0)
  return r[0].lower()+r[1:]
